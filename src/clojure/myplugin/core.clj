(ns myplugin.core
  (:gen-class)
  (:require [clojure.tools.nrepl.server :as nrepl-server]
            [cider.nrepl :refer [cider-nrepl-handler]]))

(defn start-nrepl
  "Starts a clojure repl"
  [port]
  (prn "Starting a clojure repl server at " port)
  (nrepl-server/start-server :port port
                             :handler cider-nrepl-handler))
