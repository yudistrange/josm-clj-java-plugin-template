package org.openstreetmap.josm.plugins.clojureplugintemplate;


import clojure.lang.IFn;
import clojure.lang.RT;
import clojure.lang.Symbol;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;

public class ClojurePluginTemplate extends Plugin {

    public ClojurePluginTemplate(PluginInformation info) {
        super(info);

        /**
         * Launch a Clojure repl instance on port 12345
         */
        IFn requireFn = RT.var("clojure.core", "require").fn();
        requireFn.invoke(Symbol.intern("myplugin.core"));
        IFn repl = RT.var("myplugin.core", "start-nrepl").fn();
        repl.invoke(12345);
    }
}
