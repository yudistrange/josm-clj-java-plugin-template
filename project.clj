(defproject josm-clj-plugin-template "0.0.1-SNAPSHOT"
  :description "A template on Clojure plugin for JOSM"
  :min-lein-version "2.8.1"
  :local-repo ".m2"
  :source-paths ["src/clojure"]
  :java-source-paths ["src/java"]
  :javac-options ["-target" "1.8" "-source" "1.8"]
  :repositories [["josm" {:url           "https://josm.openstreetmap.de/nexus/content/repositories/releases"
                          :snapshots     false
                          :sign-releases false
                          :checksum      :fail
                          :update        :always
                          :releases      {:checksum :fail :update :always}}]]
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.nrepl "0.2.12"]]
  :manifest {"Plugin-Mainversion"      "10160"
             "Plugin-Class"            "org.openstreetmap.josm.plugins.clojureplugintemplate.ClojurePluginTemplate"
             "Plugin-Description"      "Something I dunno what"
             "Plugin-Date"             "2018-01-18 09:23:55 +0530"
             "Author"                  "udit"
             "Plugin-Link"             "http://www.google.com"
             "Plugin-Icon"             "logo.jpg"
             "Plugin-Canloadatruntime" "true"}
  :plugins [[cider/cider-nrepl "0.16.0"]]
  :main org.openstreetmap.josm.gui.MainApplication
  :profiles {:precomp  {:source-paths ["src/clojure"]
                        :aot          [myplugin.core]}
             :provided {:dependencies [[org.openstreetmap.josm/josm "10160"]]}
             :uberjar  {:aot :all}})